import React from 'react';
import {BrowserRouter as Router, Route, Redirect, Switch} from 'react-router-dom';

import Home from '../src/home/pages/Home';
import './App.css';

const App = () => {
  return (
    <Router>
      <main>
        <Switch>
          <Route path="/" exact>
            <Home />
          </Route>
        </Switch>
      </main>
    </Router>
  );
}

export default App;
