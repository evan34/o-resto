import React  from 'react';

import Header from '../components/Header';
import NavHeader from '../components/NavHeader';
import MainHome from '../components/MainHome';
import FooterHome from '../components/FooterHome';

const Home = () => {
  return (
    <React.Fragment>
      <Header />
      <NavHeader />
      <MainHome />
      <FooterHome />
    </React.Fragment>

  )
}

export default Home;