import React from 'react';

import './MainHome.css';

const MainHome = () => {
  return (
    <React.Fragment>
      <div className="presentation-items">
        <div className="item">
          <img className="logo" src={process.env.PUBLIC_URL + '/static/logos/silverware-fork-knife.svg'} alt="logo"/>
          <h3>title</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam magnam incidunt ipsa aut corporis esse ullam aperiam exercitationem officiis obcaecati eligendi quia sunt atque dolorem, recusandae numquam! Eum, quibusdam dolorem.
          </p>
          <button>button</button>
        </div>
        <div className="item">
          <img className="logo" src={process.env.PUBLIC_URL + '/static/logos/bike-fast.svg'} alt="logo"/>
          <h3>title</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam magnam incidunt ipsa aut corporis esse ullam aperiam exercitationem officiis obcaecati eligendi quia sunt atque dolorem, recusandae numquam! Eum, quibusdam dolorem.
          </p>
          <button>button</button>
        </div>
        <div className="item">
          <img className="logo" src={process.env.PUBLIC_URL + '/static/logos/idcard-filled.svg'} alt="logo"/> 
          <h3>title</h3>
          <p>Lorem ipsum dolor sit amet consectetur adipisicing elit. Laboriosam magnam incidunt ipsa aut corporis esse ullam aperiam exercitationem officiis obcaecati eligendi quia sunt atque dolorem, recusandae numquam! Eum, quibusdam dolorem.
          </p>
          <button>button</button>
        </div>
      </div>
      <div className="image-main" >
        <div className="text-center">O BONS TAPAS</div>
        <img className="image-center" src={process.env.PUBLIC_URL + '/static/images/tapas-marbella-2.jpg'} alt="tapas"/>        
      </div>      
    </React.Fragment>
  )
}

export default MainHome;