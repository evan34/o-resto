import React from 'react';

import './NavHeader.css';

const NavHeader = () => {
  return (
    <div className="container-nav">
      <div className="link-item">Notre restaurant</div>
      <div className="link-item">La carte</div>
      <div className="link-item">Commander</div>
      <div className="link-item">Réserver une table</div>
      <div className="link-item">Contact</div>
    </div>
  )
}

export default NavHeader;