import React, { Component } from 'react'

import './FooterHome.css';

const FooterHome = () => {
    return (
      <React.Fragment>
        <div className="main-footer">
          <div className="item-footer item-footer1">
            <h4>O mon resto</h4>
            <p>Adresse rue du coin<br/>34210 vers là-bas</p>
          </div>
          <div className="item-footer item-footer2">
            <h4>Bla bla bla</h4>
            <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Porro dolor dicta quisquam dolores architecto facere adipisci rem necessitatibus cumque nemo, placeat ipsum eum labore quas omnis eligendi nobis fuga temporibus?</p>
          </div>
          <div className="item-footer item-footer3">
            <img src={process.env.PUBLIC_URL + '/static/images/map.png'} alt="map"/>
          </div>
        </div>
        <div className="footer">
          <div>TOUS DROITS RESERVES</div>
          <div>CONDITIONS GENERALES DE VENTE</div>
          <div>2020</div>
        </div>
      </React.Fragment>
    )
}

export default FooterHome
