import React from 'react';

import './Header.css';

const Header = () => {

  return (
    <div className="container">
      <img className="image" src={process.env.PUBLIC_URL + '/static/images/tapas-marbella-2.jpg'} alt="tapas"/>
      <h1 className="title">O mon resto</h1>
    </div>
  )
};

export default Header;
